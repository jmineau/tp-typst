#import "@preview/polylux:0.3.1": *
#import themes.metropolis: *

#set document(
  author: "Jack Daw", 
  title: "About the Fibonacci Sequence",
  date: datetime(
    year: 1123,
    month: 5,
    day: 8,
  )
)

#let pirat(height: 20pt) = box(move(dy: 3pt, image("rsc/logo_pirate.png", height: height)))

// Set style
#set text(font: "New Computer Modern", size: 20pt)
#show raw: set text(font: "New Computer Modern Mono", size: 20pt)
#set par(justify: true)
#show: metropolis-theme.with(
  footer: [Fibonacci for #pirat()]
)


#title-slide(
  author: [Jack Daw],
  title: [About the Fibonacci Sequence],
  date: "1123/5/8",
  extra: [
    #side-by-side[
      #move(dx: 250pt, dy: 40pt, scale(x: 170%, y: 170%)[
        #image("rsc/logo_pirate.png", width: 60%)
      ]),
    ][
      #move(dx: 310pt, dy: 125pt,
        image("rsc/head.png", width: 30%)
      )
    ]
  ]
)

#slide(title: "outline")[
  #metropolis-outline
]

#new-section-slide("Your Turn")

#slide[
  #set align(horizon+center)
  = Here you go
]
