{
  description = "TP for the paper please of 2024/02/02";
  inputs = {
    nixpkgs.url = github:NixOS/nixpkgs/nixos-23.11;
    flake-utils.url = github:numtide/flake-utils;
  };
  outputs = { self, nixpkgs, flake-utils }:
    with flake-utils.lib; eachSystem allSystems (system:
    let
      pkgs = nixpkgs.legacyPackages.${system};
      polylux = builtins.fetchurl  {
        # fetchTarball does not work because of multiple top level files
        name = "polylux";
        url = "https://packages.typst.org/preview/polylux-0.3.1.tar.gz";
        sha256 = "0728gxfdkaajky2scaa5n0ma1d3hkrxj9fr0dqjqx17pqlgh1avf"; # nix-prefetch-url <url>
      };
      pyrunner = builtins.fetchurl  {
        # fetchTarball does not work because of multiple top level files
        name = "pyrunner";
        url = "https://packages.typst.org/preview/pyrunner-0.1.0.tar.gz";
        sha256 = "0g36hvwgbwd80gd2dfay6jlqdqd8ll9aq8rijyi75gzzbf4i3bd6"; # nix-prefetch-url <url>
      };
      buildInputs = [ pkgs.typst pkgs.pdf2svg ]; # polylux2pdfpc in https://github.com/andreasKroepelin/polylux/
    in rec {
      packages = {
        document = pkgs.stdenvNoCC.mkDerivation rec {
          name = "tp_typst_an_alternative_to_latex";
          src = self;
          inherit buildInputs;
          phases = ["unpackPhase" "buildPhase" "installPhase"];
          buildPhase = ''
            export XDG_CACHE_HOME=$(mktemp -d)
            mkdir -p "$XDG_CACHE_HOME/typst/packages/preview/polylux/0.3.1"
            mkdir -p "$XDG_CACHE_HOME/typst/packages/preview/pyrunner/0.1.0"
            tar -xzf "${polylux}" -C "$XDG_CACHE_HOME/typst/packages/preview/polylux/0.3.1/"
            tar -xzf "${pyrunner}" -C "$XDG_CACHE_HOME/typst/packages/preview/pyrunner/0.1.0/"
            # for f in $(ls rsc/*.pdf); do pdf2svg $f "rsc/$(basename $f .pdf).svg"; done;
            typst compile slides.typ
            # polylux2pdfpc slides.typ
            rm -rf $XDG_CACHE_HOME
          '';
          installPhase = ''
            mkdir -p $out
            cp slides.pdf $out/
            # cp slides.pdfcp $out/
          '';
        };
      };
      defaultPackage = packages.document;
    });
}
