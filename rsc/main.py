#!/usr/bin/env python3
from pathlib import Path
from dataclasses import dataclass
from enum import Enum
import csv
import json


SVG_TEMPLATE = """<?xml version="1.0" encoding="utf-8"?>
<svg xmlns="http://www.w3.org/2000/svg" version="1.1" width="{width}" height="{height}">
  <title>{title}</title>
  <desc>{desc}</desc>
{data}
</svg>
"""


class Direction(Enum):
    UP = 0
    DOWN = 1
    LEFT = 2
    RIGHT = 3

    def next(self) -> "Direction":
        match self:
            case Direction.UP:
                return Direction.LEFT
            case Direction.LEFT:
                return Direction.DOWN
            case Direction.DOWN:
                return Direction.RIGHT
            case Direction.RIGHT:
                return Direction.UP


@dataclass
class Square:
    """Square between (x0, y0) and (x0+dx, y0+dy).
    size must be >= 0
    """

    x0: int
    y0: int
    size: int

    def svg(self, stroke: str = "black", fill: str = "none") -> str:
        """The svg code that draw the square."""
        return (
            "<rect "
            f'width="{self.size}" '
            f'height="{self.size}" '
            f'x="{self.x0}" '
            f'y="{self.y0}" '
            f'fill="{fill}" '
            f'stroke="{stroke}" '
            "/>"
        )

    def path(self, dir: Direction):
        match dir:
            case Direction.UP:
                dx = self.x0
                dy = self.y0
            case Direction.LEFT:
                dx = self.x0
                dy = self.y0 + self.size
            case Direction.DOWN:
                dx = self.x0 + self.size
                dy = self.y0 + self.size
            case Direction.RIGHT:
                dx = self.x0 + self.size
                dy = self.y0
        return f"A {self.size} {self.size} 0 0 0 {dx} {dy}"


def fibo_square(n: int) -> list[Square]:
    squares = []
    direction = Direction.UP
    for i in range(n):
        if i == 0:
            squares.append(Square(0, 0, 10))
            continue
        if i == 1:
            squares.append(Square(10, 0, 10))
            continue
        size = squares[-1].size + squares[-2].size
        match direction:
            case Direction.UP:
                x0 = squares[-2].x0
                y0 = squares[-1].y0 - size
            case Direction.LEFT:
                y0 = squares[-1].y0
                x0 = squares[-1].x0 - size
            case Direction.DOWN:
                x0 = squares[-1].x0
                y0 = squares[-1].y0 + squares[-1].size
            case Direction.RIGHT:
                x0 = squares[-1].x0 + squares[-1].size
                y0 = squares[-2].y0
        squares.append(Square(x0, y0, size))
        direction = direction.next()
    x_min = min(map(lambda r: r.x0, squares))
    y_min = min(map(lambda r: r.y0, squares))
    for square in squares:
        square.x0 -= x_min
        square.y0 -= y_min

    return squares


def golden_spiral(n: int) -> str:
    """Compute the golden spiral in svg format"""
    squares = fibo_square(n)
    width = max(map(lambda r: r.x0 + r.size, squares))
    height = max(map(lambda r: r.y0 + r.size, squares))
    svg_squares = "\n".join(map(Square.svg, squares))
    d = Direction.DOWN
    x = squares[0].x0
    y = squares[0].y0
    segments = [f"M{x} {y}"]
    for square in squares[:n]:
        segments.append(square.path(d))
        d = d.next()
    nl = "\n    "
    path = f'<path d="{nl.join(segments)}"\n    stroke="red" fill="none" />'
    data = "\n".join(map(Square.svg, squares)) + "\n" + path

    return SVG_TEMPLATE.format(
        data=data,
        title="Golden Spiral",
        desc="The golden spiral",
        width=width,
        height=height,
    )


def fibo(n: int, v0: int = 1) -> list[int]:
    """Compute the fibonacci sequence (indexed), where F_0 = F_1 = v0"""
    r = []
    for i in range(n):
        if i in [0, 1]:
            r.append(v0)
        else:
            r.append(r[-1] + r[-2])
    return r


def csv_data(fd):
    """Generate a csv file with fibonacci values"""
    writer = csv.writer(fd)
    writer.writerow(["v0"] + list(map(str, range(10))))
    for v0 in range(10):
        writer.writerow([v0] + list(map(str, fibo(10, v0))))


def json_data(fd):
    data = {"fibnocci": {f"{v0}": fibo(10, v0) for v0 in range(10)}}
    json.dump(data, fd)


if __name__ == "__main__":
    with (Path(__file__).parent / "spiral.svg").open("w") as file:
        file.write(golden_spiral(8))
    with (Path(__file__).parent / "fibo.csv").open("w", newline="") as file:
        csv_data(file)
    with (Path(__file__).parent / "fibo.json").open("w", newline="") as file:
        json_data(file)
